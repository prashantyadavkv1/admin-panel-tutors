<?php
require_once('commonConfig.php');
$objAssignmentTutorManager = ParentManagerFactory::getInstance()->getAssignmentTutorManager();
$reviewerName = CommonFunc::escapeData($_POST['reviewerName']);
$reviewRating = CommonFunc::escapeData($_POST['reviewRating']);
$reviewHeading  = CommonFunc::escapeData($_POST['reviewHeading']);
$reviewDescription = CommonFunc::escapeData($_POST['reviewDescription']);
$reviewDate = CommonFunc::escapeData($_POST['reviewDate']);
$tutorMainSubject = CommonFunc::escapeData($_POST['tutorMainSubject']);
$tutorReply = CommonFunc::escapeData($_POST['tutorReply']);
//echo $reviewerName;die;
$insertReviewData = $objAssignmentTutorManager->addReview($reviewerName,$reviewRating,$reviewHeading,$reviewDescription,$reviewDate,$tutorMainSubject,$tutorReply);

header('location:addReview.php');
