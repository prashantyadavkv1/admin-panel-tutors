<?php
$tutorID = $_GET['tutorID'];
require_once('commonConfig.php');
$objAssignmentTutorManager = ParentManagerFactory::getInstance()->getAssignmentTutorManager();
$deleteTutor = $objAssignmentTutorManager->deleteTutor($tutorID);
header('location:tutorList.php');
