<?php
$reviewID = $_GET['reviewID'];
require_once('commonConfig.php');
$objAssignmentTutorManager = ParentManagerFactory::getInstance()->getAssignmentTutorManager();
$deleteTutor = $objAssignmentTutorManager->deleteTutorReview($reviewID);
header('location:reviewList.php');
