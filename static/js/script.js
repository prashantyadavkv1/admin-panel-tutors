$( function() {
  $( "#datepicker" ).datepicker({
    changeMonth: true,
      changeYear: true});
    $( "#datepicker" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
} );

$(document).ready( function(){
$('.summernote').summernote({
    toolbar: [
    ['style', ['bold', 'italic', 'underline']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol']],
    ],
    height: 300,
    disableDragAndDrop: true
});
});
function validateReviewForm() {
  var error = false;
  var errMsg = "";
  var reviewerName = $('#reviewerName').val();
  if (reviewerName.match('^[a-zA-Z]{3,90}$')) {
       error = true;
       //$(#errName).html("Enter valid review");
   } else {
//$(#errName).html("");
   }
   if(error) {
     return false;
   }
   else {
  return true;
}
}
