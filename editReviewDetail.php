<?php
require_once('commonConfig.php');
$objAssignmentTutorManager = ParentManagerFactory::getInstance()->getAssignmentTutorManager();
$reviewID = CommonFunc::escapeData($_POST['reviewID']);
$reviewerName = CommonFunc::escapeData($_POST['reviewerName']);
$reviewRating = CommonFunc::escapeData($_POST['reviewRating']);
$reviewHeading  = CommonFunc::escapeData($_POST['reviewHeading']);
$reviewDescription = CommonFunc::escapeData($_POST['reviewDescription']);
$reviewDate = CommonFunc::escapeData($_POST['reviewDate']);
$mainSubject = CommonFunc::escapeData($_POST['mainSubject']);
$tutorReply = CommonFunc::escapeData($_POST['tutorReply']);
//echo $reviewID;die;
$updateReviewData = $objAssignmentTutorManager->updateReviewDetail($reviewID, $reviewerName, $reviewRating, $reviewHeading, $reviewDescription, $reviewDate, $mainSubject, $tutorReply);
header('location:addReview.php');
