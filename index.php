<?php
require_once('commonConfig.php');
$objAssignmentTutorManager = ParentManagerFactory::getInstance()->getAssignmentTutorManager();
$tutorCountCurrentDate = $objAssignmentTutorManager->getTutorCountByCurrentDate();
$tutorCountAll = $objAssignmentTutorManager->getAllTutorCount();
$reviewCountCurrentDate = $objAssignmentTutorManager->getReviewCountByCurrentDate();
$reviewCountAll = $objAssignmentTutorManager->getAllReviewCount();
$smarty->assign('tutorCountCurrentDate', $tutorCountCurrentDate);
$smarty->assign('tutorCountAll', $tutorCountAll);
$smarty->assign('reviewCountCurrentDate', $reviewCountCurrentDate);
$smarty->assign('reviewCountAll', $reviewCountAll);
$smarty->display('index.tpl');
