~include file="header.tpl"`
~include file="layout.tpl"`
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Add Tutor</h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <form role="form" action="addTutorDetail.php" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Name</label>
                      <input type="text" class="form-control" name="name"  ~if $country.countryID eq $tutorCountry`  selected  ~/if`placeholder="Please Enter Your Name" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Main Subject</label>
                      <select class="form-control select2" style="width: 100%;" name="subject" required>
                          ~foreach from = $arrMainSubject key=key item=subject`
                          <option value="~$subject.mainSubjectID`">~$subject.mainSubjectValue`</option>
                        		~/foreach`
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Country</label>
                      <select class="form-control select2" style="width: 100%;" name="country" required>
                        ~foreach from = $arrCountryList key=key1 item=country`
                      <option value="~$country.countryID`">~$country.countryNanme`</option>
                    ~/foreach`
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Rating</label>
                      <select class="form-control select2" style="width: 100%;" name="rating" required>
                        <option value="3.9">3.9</option>
                        <option value="4.0">4.0</option>
                        <option value="4.1">4.1</option>
                        <option value="4.2">4.2</option>
                        <option value="4.3">4.3</option>
                        <option value="4.4">4.4</option>
                        <option value="4.5">4.5</option>
                        <option value="4.6">4.6</option>
                        <option value="4.7">4.7</option>
                        <option value="4.8">4.8</option>
                        <option value="4.9">4.9</option>
                        <option value="5.0">5.0</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <textarea name="description" class="summernote" placeholder="Enter Tutor Description" required></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Order Finished</label>
                      <input type="number" class="form-control" placeholder="Enter Number of Projects" name="orderFinshed" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Order Process</label>
                      <select class="form-control select2" style="width: 100%;" name="orderProcess" required>
                        ~for $count=1 to 10`
                        <option value="~$count`">~$count`</option>
                        ~/for`
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Tutor Image</label>
                  <input type="file" id="exampleInputFile" name="image" accept="image/x-png" required>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Recently Updated Tutors</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Tutor Name</th>
                  <th>Tutor Main Subject</th>
                  <th>Tutor Country</th>
                  <th>Tutor Rating</th>
                  <!--<th>Tutor Description</th>-->
                  <th>Tutor Order Finished</th>
                  <th>Tutor Order Processing</th>
                  <th>Tutor Edit</th>
                  <th>Tutor Delete</th>
                </tr>
                ~foreach from = $arrTutorData key=key item=tutorData`
                <tr>

                  <td>~$tutorData.tutorName`</td>
                  <td>~$tutorData.valueSubject`</td>
                  <td>~$tutorData.valueCountry`</td>
                  <td>~$tutorData.tutorRating`</td>
                  <!--<td>~$tutorData.tutorDescription`</td>-->
                  <td>~$tutorData.orderFinished`</td>
                  <td>~$tutorData.orderProcessing`</td>
                  <td><a href="editTutor.php?tutorID=~$tutorData.tutorID`">Edit</a></td>
                  <td><a href="deleteTutor.php?tutorID=~$tutorData.tutorID`" onclick="return confirm('Are you sure to delete this record');">Delete</a></td>
                </tr>
                ~/foreach`
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
~include file="footer.tpl"`
