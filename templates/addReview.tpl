~include file="header.tpl"`
~include file="layout.tpl"`
<div class="content-wrapper">
    <section class="content-header">
      <h1>Add Review</h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <form role="form" action="addReviewDetail.php" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Reviewer Name</label>
                      <input type="text" class="form-control" placeholder="Please Enter Reviewer Name" name="reviewerName" id="reviewerName" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Subject</label>
                      <select class="form-control select2" style="width: 100%;" name="tutorMainSubject" required>
                        ~foreach from = $arrMainSubject key=key item=subject`
                        <option value="~$subject.mainSubjectID`">~$subject.mainSubjectValue`</option>
                      		~/foreach`
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Rating</label>
                      <select class="form-control select2" style="width: 100%;" name="reviewRating" required>
                        <option selected="selected">3.9</option>
                        <option value="3.9">3.9</option>
                        <option value="4.0">4.0</option>
                        <option value="4.1">4.1</option>
                        <option value="4.2">4.2</option>
                        <option value="4.3">4.3</option>
                        <option value="4.4">4.4</option>
                        <option value="4.5">4.5</option>
                        <option value="4.6">4.6</option>
                        <option value="4.7">4.7</option>
                        <option value="4.8">4.8</option>
                        <option value="4.9">4.9</option>
                        <option value="5.0">5.0</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Review Heading</label>
                      <input type="text" class="form-control" placeholder="Please Enter Review Heading" name="reviewHeading" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Write Review</label>
                      <textarea class="form-control" rows="3" placeholder="Enter Comment" name="reviewDescription" required></textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Date:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker" name="reviewDate" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Reply</label>
                      <textarea class="form-control" rows="3" placeholder="Enter Tutor Reply" name="tutorReply"></textarea>
                    </div>
                  </div>
                  <div class="col-md-6"></div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Recently Added Reviews</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Reviewer Name</th>
                  <th>Tutor Main Subject</th>
                  <th>Review Rating</th>
                  <th>Review Heading</th>
                  <th>Review Description</th>
                    <th>Review Date</th>
                  <th>Tutor Tutor Reply</th>
                  <th>Review Edit</th>
                  <th>Review Delete</th>
                </tr>
                ~foreach from = $arrReviewData key=key item=reviewData`
              <tr>
                <td>~$reviewData.reviewerName`</td>
                <td>~$reviewData.mainSubjectValue`</td>
                <td>~$reviewData.reviewRating`</td>
                <td>~$reviewData.reviewHeading`</td>
                <td>~$reviewData.reviewDescription`</td>
                <td>~$reviewData.reviewDate`</td>
                <td>~$reviewData.reviewTutorReply`</td>
                <td><a href="editReview.php?reviewID=~$reviewData.tutorReviewID`">Edit</a></td>
                <td><a href="deleteReview.php?reviewID=~$reviewData.tutorReviewID`" onclick="return confirm('Are you sure to delete this record');">Delete</a></td>
              </tr>
              ~/foreach`
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  ~include file="footer.tpl"`
