~include file="header.tpl"`
~include file="layout.tpl"`
<div class="content-wrapper">
    <section class="content-header">
      <h1>Review Lists</h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Recently Added Reviews</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Reviewer Name</th>
                  <th>Reviewer Main Subject</th>
                  <th>Review Rating</th>
                  <th>Review Heading</th>
                  <th>Review Description</th>
                    <th>Review Date</th>
                  <th> Tutor Reply</th>
                  <th>Review Edit</th>
                  <th>Review Delete</th>
                </tr>
                ~foreach from = $arrReviewData key=key item=reviewData`
              <tr>
                <td>~$reviewData.reviewerName`</td>
                <td>~$reviewData.mainSubjectValue`</td>
                <td>~$reviewData.reviewRating`</td>
                <td>~$reviewData.reviewHeading`</td>
                <td>~$reviewData.reviewDescription`</td>
                <td>~$reviewData.reviewDate`</td>
                <td>~$reviewData.reviewTutorReply`</td>
                <td><a href="editReview.php?reviewID=~$reviewData.tutorReviewID`">Edit</a></td>
                <td><a href="deleteReview.php?reviewID=~$reviewData.tutorReviewID`" onclick="return confirm('Are you sure to delete this record');">Delete</a></td>
              </tr>
              ~/foreach`

              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
    ~include file="footer.tpl"`
