~include file="header.tpl"`
~include file="layout.tpl"`
<div class="content-wrapper">
  <section class="content-header">
    <h1>Add Review</h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <form role="form" action="editReviewDetail.php" method="post" enctype="multipart/form-data" >
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Reviewer Name</label>
                    <input type="text" class="form-control" value="~$reviewName`" name="reviewerName" id="reviewerName" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Subject</label>
                    <select class="form-control select2" style="width: 100%;" name="mainSubject" required>
                      ~foreach from = $arrMainSubject key=key item=subject`
                      <option value="~$subject.mainSubjectID`" ~if $subject.mainSubjectID eq $reviewSubjectID`  selected  ~/if`>~$subject.mainSubjectValue`</option>
                      ~/foreach`
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Rating</label>
                    <select class="form-control select2" style="width: 100%;" name="reviewRating" required>
                      <option value="3.9" ~if $reviewRating eq 3.9`  selected  ~/if`>3.9</option>
                      <option value="4.0" ~if $reviewRating eq 4.0`  selected  ~/if`>4.0</option>
                      <option value="4.1" ~if $reviewRating eq 4.1`  selected  ~/if`>4.1</option>
                      <option value="4.2" ~if $reviewRating eq 4.2`  selected  ~/if`>4.2</option>
                      <option value="4.3" ~if $reviewRating eq 4.3`  selected  ~/if`>4.3</option>
                      <option value="4.4" ~if $reviewRating eq 4.4`  selected  ~/if`>4.4</option>
                      <option value="4.5" ~if $reviewRating eq 4.5`  selected  ~/if`>4.5</option>
                      <option value="4.6" ~if $reviewRating eq 4.6`  selected  ~/if`>4.6</option>
                      <option value="4.7" ~if $reviewRating eq 4.7`  selected  ~/if`>4.7</option>
                      <option value="4.8" ~if $reviewRating eq 4.8`  selected  ~/if`>4.8</option>
                      <option value="4.9" ~if $reviewRating eq 4.9`  selected  ~/if`>4.9</option>
                      <option value="5.0" ~if $reviewRating eq 5.0`  selected  ~/if`>5.0</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Review Heading</label>
                    <input type="text" class="form-control" value="~$reviewHeading`" name="reviewHeading" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Write Review</label>
                    <textarea class="form-control" rows="3" name="reviewDescription" required>~$reviewDescription`</textarea>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Date:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" value="~$reviewDate`" class="form-control pull-right" id="datepicker2" name="reviewDate" required>
                    </div>
                  </div>
                </div>
              </div>
              <input type="hidden" name="reviewID" value="~$reviewID`">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Tutor Reply</label>
                    <textarea class="form-control" rows="3" name="tutorReply">~$reviewTutorReply`</textarea>
                  </div>
                </div>
                <div class="col-md-6"></div>
              </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
</div>
~include file="footer.tpl"`
