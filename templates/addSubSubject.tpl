~include file="header.tpl"`
~include file="layout.tpl"`
<div class="content-wrapper">
    <section class="content-header">
      <h1>Add Sub Subject</h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <form role="form" action="addSubSubjectDetail.php" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Sub Subject</label>
                      <input type="text" class="form-control" placeholder="Please Enter Sub Subject" name="subSubjectName" required>
                    </div>
                  </div>  </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Main Subject</label>
                  <select class="form-control select2" style="width: 100%;" name="mainSubject" required>
                      ~foreach from = $arrMainSubject key=key item=subject`
                      <option value="~$subject.mainSubjectID`">~$subject.mainSubjectValue`</option>
                        ~/foreach`
                  </select>
                </div>
              </div>
              <!-- /.box-body -->
  <div class="box-body">
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sub Subject List</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Main Subject Name</th>

                </tr>
                ~foreach from = $arrSubSubject key=key item=subject`
              <tr>
                <td>~$subject.subSubjectValue`</td>

              </tr>
              ~/foreach`
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  ~include file="footer.tpl"`
