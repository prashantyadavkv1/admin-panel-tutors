~include file="header.tpl"`
~include file="layout.tpl"`
<div class="content-wrapper">
    <section class="content-header">
      <h1>Tutor Lists</h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Tutors</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th>Tutor Name</th>
                  <th>Tutor Main Subject</th>
                  <th>Tutor Country</th>
                  <th>Tutor Rating</th>
                  <!--<th>Tutor Description</th>-->
                  <th>Tutor Order Finished</th>
                  <th>Tutor Order Processing</th>
                  <th>Tutor Edit</th>
                  <th>Tutor Delete</th>
                </tr>
                ~foreach from = $arrTutorData key=key item=tutorData`
                <tr>

                  <td>~$tutorData.tutorName`</td>
                  <td>~$tutorData.mainSubjectValue`</td>
                  <td>~$tutorData.countryNanme`</td>
                  <td>~$tutorData.tutorRating`</td>
                  <!--<td>~$tutorData.tutorDescription`</td>-->
                  <td>~$tutorData.orderFinished`</td>
                  <td>~$tutorData.orderProcessing`</td>
                  <td><a href="editTutor.php?tutorID=~$tutorData.tutorID`">Edit</a></td>
                  <td><a href="deleteTutor.php?tutorID=~$tutorData.tutorID`" onclick="return confirm('Are you sure to delete this record');">Delete</a></td>
                </tr>
                ~/foreach`

              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
~include file="footer.tpl"`
