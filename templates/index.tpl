~include file="header.tpl"`
~include file="layout.tpl"`
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Tutor <small>Control panel</small></h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>~$tutorCountAll`</h3>
            <p>Tutor Counts</p>
          </div>
          <div class="icon"><i class="ion ion-bag"></i></div>
          <a href="tutorList.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>~$tutorCountCurrentDate`</h3>
            <p>Tutor Added Today</p>
          </div>
          <div class="icon"><i class="ion ion-stats-bars"></i></div>
          <a href="tutorAddToday.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->

  <section class="content-header">
    <h1>Review <small>Control panel</small></h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>~$reviewCountAll`</h3>
            <p>Total Review</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="reviewList.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>~$reviewCountCurrentDate`</h3>
            <p>Review Added Today</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="reviewAddToday.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->

</div>
~include file="footer.tpl"`
