~include file="header.tpl"`
~include file="layout.tpl"`
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Add Tutor</h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <form role="form" action="editTutorDetail.php" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Name</label>
                      <input type="text" class="form-control" name="name" placeholder="Please Enter Your Name" value="~$tutorName`" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Main Subject</label>
                      <select class="form-control select2" style="width: 100%;" name="subject" required>
                          ~foreach from = $arrMainSubject key=key item=subject`
                          <option value="~$subject.mainSubjectID`" ~if $subject.mainSubjectID eq $tutorMainSubjectID`  selected  ~/if`>~$subject.mainSubjectValue`</option>
                        		~/foreach`
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Country</label>
                      <select class="form-control select2" style="width: 100%;" name="country" required>
                        ~foreach from = $arrCountryList key=key1 item=country`
                      <option value="~$country.countryID`"  ~if $country.countryID eq $tutorCountry`  selected  ~/if`>~$country.countryNanme`</option>
                    ~/foreach`
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tutor Rating</label>
                      <select class="form-control select2" style="width: 100%;" name="rating" required>
                        <option value="3.9" ~if $tutorRating eq 3.9`  selected  ~/if`>3.9</option>
                        <option value="4.0" ~if $tutorRating eq 4.0`  selected  ~/if`>4.0</option>
                        <option value="4.1" ~if $tutorRating eq 4.1`  selected  ~/if`>4.1</option>
                        <option value="4.2" ~if $tutorRating eq 4.2`  selected  ~/if`>4.2</option>
                        <option value="4.3" ~if $tutorRating eq 4.3`  selected  ~/if`>4.3</option>
                        <option value="4.4" ~if $tutorRating eq 4.4`  selected  ~/if`>4.4</option>
                        <option value="4.5" ~if $tutorRating eq 4.5`  selected  ~/if`>4.5</option>
                        <option value="4.6" ~if $tutorRating eq 4.6`  selected  ~/if`>4.6</option>
                        <option value="4.7" ~if $tutorRating eq 4.7`  selected  ~/if`>4.7</option>
                        <option value="4.8" ~if $tutorRating eq 4.8`  selected  ~/if`>4.8</option>
                        <option value="4.9" ~if $tutorRating eq 4.9`  selected  ~/if`>4.9</option>
                        <option value="5.0" ~if $tutorRating eq 5.0`  selected  ~/if`>5.0</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <textarea name="description" class="summernote" placeholder="Enter Tutor Description" required>~$tutorDescription`</textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Order Finished</label>
                      <input type="number" class="form-control" value="~$orderFinished`" name="orderFinshed" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Order Process</label>
                      <select class="form-control select2" style="width: 100%;" name="orderProcess" required>
                        ~for $count=1 to 10`
                       <option value="~$count`" ~if $count eq $orderProcessing`  selected  ~/if`>~$count`</option>
                        ~/for`
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Tutor Image</label>
                <img src="../../live/uploads/~$tutorID`.png" width="200" height="150">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Change Image</label>
                  <input type="file" id="exampleInputFile" name="image" accept="image/x-png">
                </div>

              </div>
                <input type="hidden" name="tutorID" value="~$tutorID`">
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>

  </div>
~include file="footer.tpl"`
