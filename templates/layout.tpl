<aside class="main-sidebar">
  <section class="sidebar">
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="active "><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Tutor</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class=""><a href="addTutor.php"><i class="fa fa-circle-o"></i> Add Tutor</a></li>
          <li><a href="tutorList.php"><i class="fa fa-circle-o"></i>Tutor List</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">

          <i class="fa fa-dashboard"></i> <span>Reviews</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="fa fa-circle-o"><a href="addReview.php"><i class="fa fa-circle-o"></i> Add Reviews</a></li>
          <li><a href="reviewList.php"><i class="fa fa-circle-o"></i> Reviews List</a></li>
        </ul>
      </li>
      <li><a href="addMainSubject.php"><i class="fa fa-dashboard"></i> <span>Add Main Subject</span></a></li>
      <li><a href="addSubSubject.php"><i class="fa fa-dashboard"></i> <span>Add Sub Subject</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
