<?php
require_once('commonConfig.php');
$objAssignmentTutorManager = ParentManagerFactory::getInstance()->getAssignmentTutorManager();

$tutorName = CommonFunc::escapeData($_POST['name']);
$tutorSubject = CommonFunc::escapeData($_POST['subject']);
$tutorCountry  = CommonFunc::escapeData($_POST['country']);
$tutorRating = CommonFunc::escapeData($_POST['rating']);
$tutorDescription = $_POST['description'];
$tutorOrderFinished = CommonFunc::escapeData($_POST['orderFinshed']);
$tutorOrderProcess = CommonFunc::escapeData($_POST['orderProcess']);
$tutorImage = $_FILES["image"];

$insertTutorData = $objAssignmentTutorManager->addTutor($tutorName,$tutorSubject,$tutorCountry,$tutorRating,$tutorDescription,$tutorOrderFinished,$tutorOrderProcess,$tutorImage);
header('location:addTutor.php');
